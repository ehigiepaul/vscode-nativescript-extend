# Change Log

<!-- ## [Unreleased]
- Initial release -->

## [Unreleased]

- Nativescript language server (coming soon)
- Nativescript editor (coming soon)
- Navivescript Preview (coming soon)
- Nativescript intellisense (coming soon)

## [1.10.27] - 2018-10-27

- Newly added my updated Nativescript xsd schema, which will help you with Nativescript XML widgets, attributes and parameters validation [BETA STAGE].
- Added Auto setup emulator. It works only if you have android path on your system path or enviroment.
- Fixed file naming to all lowercase characters.
- Added "redhat.vscode-xml" extension to support xml validaion.
- Added "octref.vetur" extension to support vue sfc.
- Updated the `README.MD`

## [1.10.21] - 2018-10-21

- Improved Smart Page creation for Nativescript projects only
- Fixed emulator to detect nativescript projects
- Added more attribute snippets
- Added more parameter to snippets
- Added plugin inteligent.
- Updated the `README.MD`
- Update the explorer context menu. It now creates a Page folder for every page created except for VUE which is a SFC

## [1.10.17] - 2018-10-17

- Android emulator static which doesnt allow more than one emulator.
- Change Android status bar text and color
- Fixed vue page content format.

## [1.10.16] - 2018-10-16

- Made changes to the documentaion

## [1.10.15] - 2018-10-15

- Fixed bugs
- Added inspired projects
